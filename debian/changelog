repo (2.14.3-1) UNRELEASED; urgency=medium

  [ Manas Kashyap ]
  * Team upload 
  * New upstream version 2.14.3

  [ Roger Shimizu ]
  * Update bash_completion for repo

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Sat, 08 May 2021 12:27:58 +0000

repo (2.13.8-1) unstable; urgency=medium

  * New upstream release 2.13.8
  * debian/watch:
    - Update filenamemangle rule.
  * debian/help2man/*.1:
    - Update manpages by script.

 -- Roger Shimizu <rosh@debian.org>  Sun, 25 Apr 2021 14:56:21 +0900

repo (2.12-1) unstable; urgency=medium

  * New upstream release 2.12
  * d/help2man/*.1: Update by script

 -- Roger Shimizu <rosh@debian.org>  Wed, 27 Jan 2021 02:12:14 +0900

repo (2.11.1-1) unstable; urgency=medium

  * New upstream release 2.11.1
  * debian/patches:
    - Refresh patches.
  * d/help2man/*.1: Update by script

 -- Roger Shimizu <rosh@debian.org>  Sat, 09 Jan 2021 18:32:07 +0900

repo (2.10-1) unstable; urgency=medium

  * New upstream release v2.10
  * debian/control:
    - Bump up git depends version to 2.20
  * debian/patches:
    - Refresh python shebang patch.
    - Cherry-pick 2 patches from upstream to better support for python3.
  * debian/help2man:
    - Update manpages.

 -- Roger Shimizu <rosh@debian.org>  Thu, 03 Dec 2020 21:24:12 +0900

repo (2.9-1) unstable; urgency=medium

  * New upstream version 2.9
  * debian/help2man/update-help:
    - Update the title of each manpages.
  * debian/*lintian-overrides:
    - Update manpages related stuff.
  * debian/patches:
    - Drop upstreamed patches.

 -- Roger Shimizu <rosh@debian.org>  Fri, 11 Sep 2020 01:57:40 +0900

repo (2.8-3) unstable; urgency=medium

  * debian/help2man:
    - Remove the additional footer of help2man generated manpages.
  * debian/patches:
    - Cherry-pick 0003 patch to fix typo.
    - Move upstreamed patches to d/patches/backport folder.
  * Update debian/help2man/repo-sync.1 to fix typo.

 -- Roger Shimizu <rosh@debian.org>  Wed, 01 Jul 2020 02:55:14 +0900

repo (2.8-2) unstable; urgency=medium

  * debian/patches:
    - Amend 0002 patch with the info from upstream, and it already
      got merged.
  * debian/help2man/update-help, debian/repo.manpages, debian/*.1:
    - Move generated manpages to debian/help2man directory.
    - Add more manpages of repo sub-commands.
  * debian/lintian-overrides:
    - Repo sub-commands don't have binary, so it's safe to add them
      into lintian-overrides.
  * debian/copyright:
    - Remove d/repo.1 section, since now we generate it by script.

 -- Roger Shimizu <rosh@debian.org>  Tue, 09 Jun 2020 00:53:32 +0900

repo (2.8-1) unstable; urgency=medium

  * New upstream version 2.8
  * Add script debian/help2man/update-help to genernate manpages.
  * debian/patches:
    - Add 0002 patch to fix typo in help.
  * debian/*.1:
    - Update manpages, generated from script.
  * debian/repo.manpages:
    - Add new manpages to install.

 -- Roger Shimizu <rosh@debian.org>  Sat, 06 Jun 2020 03:10:13 +0900

repo (2.7-1) unstable; urgency=medium

  * New upstream version 2.7
  * debian/control:
    - Add myself to uploaders.
    - Add dh-python and python3-setuptools as B-D.
  * debian/rules:
    - Use pybuild as buildsystem.
    - Disable dh_auto_test since it fails.
  * debian/patches:
    - Update the shebang patch to include all related scripts, so that
      we can safely drop dependency to python2.
  * debian/tests/control:
    - Update test command to include execution time output.

 -- Roger Shimizu <rosh@debian.org>  Sun, 10 May 2020 22:51:00 +0900

repo (1.13.7-2) unstable; urgency=medium

  * Team upload.
  * Change autopkgtest to not negate repo help return code.
    Thanks to Michel Le Bihan for merge-request on salsa.
    (Closes: #949461)
  * debian/control: Add Rules-Requires-Root: no
  * Add debian/upstream/metadata.
  * debian/tests/control: Add ca-certificates to dependency.

 -- Roger Shimizu <rosh@debian.org>  Fri, 08 May 2020 22:05:19 +0900

repo (1.13.7-1) unstable; urgency=medium

  [ Nicolas Braud-Santoni ]
  * New upstream version 1.13.7
  * Switch to Python 3  (Closes: #938352)
  * debian/copyright: Fix ordering of paragraphs
  * debian/control: Declare compliance with policy v4.4.1
  * Upgrade to dh compatibility level 12

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 18 Oct 2019 09:41:14 +0200

repo (1.13.2-1) unstable; urgency=medium

  * New upstream version
  * Add basic autopkgtest

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 17 Jan 2019 20:57:57 +0100

repo (1.12.37-3) unstable; urgency=medium

  * install bash-completion with debhelper (Closes: #856417)

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 28 Feb 2017 21:51:18 +0100

repo (1.12.37-2) unstable; urgency=medium

  * move to contrib, it violates policy 2.2.1 (Closes: #855846)

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 24 Feb 2017 23:59:16 +0100

repo (1.12.37-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 3.9.8

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 18 Dec 2016 21:45:53 +0100

repo (1.12.32-2) unstable; urgency=medium

  * add bash completion
  * manpage based on output of `repo help --all` (Closes: #806546)
  * document other sources of documentation
  * remove obsolete section from README.source

 -- Hans-Christoph Steiner <hans@eds.org>  Sun, 27 Dec 2015 23:25:16 +0100

repo (1.12.32-1) unstable; urgency=medium

  * New upstream release
  * Use get-orig-source to fetch upstream tarball

 -- Kai-Chung Yan <seamlikok@gmail.com>  Mon, 07 Dec 2015 11:59:59 +0800

repo (1.12.22-1) unstable; urgency=low

  * Initial Release (Closes: #792014)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 09 Jul 2015 23:37:57 -0700
